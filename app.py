# py -3 -m pip install Flask  (install libraries in python 3.9 syntax)

from flask import Flask, render_template, jsonify, request,redirect
#from flask_cors import CORS 
from flask import *
import json
from flask_cors import CORS, cross_origin
import os
import requests
#import mysql.connector
import pymysql



app = Flask(__name__)
app.static_folder = 'static'
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
server_reponse={'ack':"",'data':""}

'''
we do this because we dont know where the docker image will be hosted hence we cannot fix the ipaddress for the APIs,
so we get the ip address of the server and write it into config.js file. 
'''

try:

# Get the machine's IP address
	print("grtting ip")
	response = requests.get('https://api.ipify.org?format=json')
	data = response.json()
	public_ip = data['ip']
	#public_ip = "123"


	print("Public IP Address:", public_ip)
	#  Write the IP address to a JavaScript file
	javascript_code = f'var server_ip = "http://{public_ip}:5000/";'

	with open("static/js/config.js", "w") as js_file:
	    js_file.write(javascript_code)
except ConnectionError:
	print("connect error to find public ip address.")

mydb = pymysql.connect(
  host="db",
  #host="localhost",
  user="root",
  password="root"
)
try:
    mycursor = mydb.cursor()
    print("DB connected")
except:
    print("DB not connected")

@cross_origin()

@app.route("/")
def home():
    return render_template("index.html")

@app.route("/admin")
def admin():
    return render_template("admin.html")


@app.route("/client")
def client():
    return render_template("client.html")

@app.route("/login")
def login_page():
    return render_template("login.html")

@app.route("/img1_360")
def img1_360():
    return render_template("img_360_1.html")

@app.route("/img2_360")
def img2_360():
    return render_template("img_360_2.html")

@app.route("/img3_360")
def img3_360():
    return render_template("img_360_3.html")

@app.route("/img4_360")
def img4_360():
    return render_template("img_360_4.html")

@app.route("/img5_360")
def img5_360():
    return render_template("img_360_5.html")

@app.route("/test")
def test():
    return ("hello, am running")

@app.route("/sign_up", methods=['POST'])
def sign_up():
	response={"ack": ""}
	#try:
	data = request.data  
	print(">>",data)        
	reg_data = json.loads(data)
	# INSERT INTO `aflatoxin`.`registration` (`name`, `email`, `phone`, `password`) VALUES ('sahique', 'msahiqe@gmail.com', '8880655639', 's12345');
	sql="INSERT INTO aflatoxin.registration (name,email,phone,password,role) VALUES (%s,%s,%s,%s,%s);"   
	values=( reg_data['name'],reg_data['email'],reg_data['contact'],reg_data['password'],reg_data['role'],)      
	mycursor.execute(sql,values)
	mydb.commit()
	print(mycursor.rowcount, "was inserted.")
	if (mycursor.rowcount==1): response['ack']="succesfull"
	else :  response['ack']="failed"
	return jsonify(response)
	

@app.route("/login", methods=['POST'])
def login():
	response={"ack": ""}
	#try:
	data = request.data  
	print(">>",data)        
	reg_data = json.loads(data)
	print(reg_data['email'],reg_data['password'])
	# INSERT INTO `aflatoxin`.`registration` (`name`, `email`, `phone`, `password`) VALUES ('sahique', 'msahiqe@gmail.com', '8880655639', 's12345');
	sql="SELECT * FROM aflatoxin.registration WHERE email= %s  AND password= %s ;" 
	values=( reg_data['email'],reg_data['password'],)      
	mycursor.execute(sql,values)
	myresult = mycursor.fetchall()
	print("myres:",myresult)
	if (len(myresult)==1):  response['ack']="succesfull"; response['data']=myresult;  
	else :  response['ack']="failed"
	return jsonify(response)
	
	
def check_filename(path,filename):
	# Check if the file already exists
	print(path,filename)
	if os.path.exists(path+filename):
		print(f"File {filename} already exists. Generating a new filename.")

		# Generate a new unique filename
		unique_id = 1
		while True:
			new_filename = f"{filename}_{unique_id}.json"
			if not os.path.exists(path+new_filename):
				break
			unique_id += 1

		filename = new_filename
	return filename	

def write_into_json_file(data_to_write):
	# Serializing json
	json_object = json.dumps(data_to_write, indent=4)
	path="./static/results/"
	# Writing to sample.json
	if ('/' in data_to_write['title']):
		
		new_data_to_write=data_to_write['title'].replace('/','_')
		filename=new_data_to_write+"_"+data_to_write['user']+".json"
		filename=check_filename(path,filename)
		with open(path+filename, "w") as outfile:
			outfile.write(json_object)
	else:
		filename=data_to_write['title']+"_"+data_to_write['user']+".json"
		filename=check_filename(path,filename)
		with open(path+filename, "w") as outfile:
			outfile.write(json_object)
	ack = ""
	sql="INSERT INTO aflatoxin.answer_sheet (qp_tittle, client_name, qp_filename) VALUES (%s,%s,%s);"   
	values=( data_to_write['title'], data_to_write['user'], filename,)      
	mycursor.execute(sql,values)
	mydb.commit()
	print(mycursor.rowcount, "was inserted.")
	if (mycursor.rowcount==1): ack="succesfull"
	else: ack="Failed"
	return ack
# API to collect the result of the survey
@app.route('/collect_data',methods=['POST'])
def collection():
    #try:
    data = request.data  
    print(">>",data)        
    j_req_data = json.loads(data)
    res=write_into_json_file(j_req_data)
    server_reponse['ack']=res
    return jsonify(server_reponse)

# API to collect the questionniare
@app.route('/collect_questionnaire',methods=['POST'])
def collect_questionnaire():
	response={"ack": ""} 
	filename=""
	#try:
	data = request.data  
	print(">>",data)        
	j_req_data = json.loads(data)
	# Serializing json
	json_object = json.dumps(j_req_data, indent=4)
	#print(j_req_data['pages'][0]['title'])
	path="./static/Questionnaire/"
	if ('/' in j_req_data['pages'][0]['title']):
		new_j_req_data=j_req_data['pages'][0]['title'].replace('/','_')
		filename=new_j_req_data+".json"
		filename=check_filename(path,filename)
		with open(path+filename, "w") as outfile:
			outfile.write(json_object)
	else:
		filename=j_req_data['pages'][0]['title']+".json"
		filename=check_filename(path,filename)
		with open(path+filename, "w") as outfile:
			outfile.write(json_object)

	sql="INSERT INTO aflatoxin.question_papers (author, qp_title, qp_filename) VALUES (%s,%s,%s);"   
	values=( j_req_data['pages'][0]['Author'], j_req_data['pages'][0]['title'], filename,)      
	mycursor.execute(sql,values)
	mydb.commit()
	print(mycursor.rowcount, "was inserted.")
	if (mycursor.rowcount==1): response['ack']="succesfull"
	else: response['ack']="Failed"

	return jsonify(server_reponse)

@app.route('/listfiles', methods=['POST'])
def listfiles():
	basepath = './static/results'
	flist=""
	first=0
	for entry in os.scandir(basepath):
		if entry.is_dir():    # skip directories
			continue
		else:
			if first==0:
				first=1
				flist=entry.name
			else:
				flist=flist+","+entry.name
	print("files=", flist) # use entry.path to get the full path of this entry, or use entry.name for the base filename
	if (len(flist)>0):
		server_reponse['ack']="List of files found"; server_reponse['data']=flist
	else:
		server_reponse['ack']="no files found"
	return jsonify(server_reponse)

@app.route('/getfile', methods=['POST'])
def getfile():
	data = request.data    
	print(">>",data)  
	y = json.loads(data)
	print(y)
	print(y['file'])
	import os.path
	file_path='./static/results/'+y['file']
	file_exists = os.path.exists(file_path)
	if(file_exists==True):
		f = open(file_path)
		file_data = json.load(f)
		server_reponse['ack']="file found successfully"
		server_reponse['data']=file_data
	else:
		server_reponse['ack']="file not found"
	
	return jsonify(server_reponse)

@app.route('/get_questionnaire', methods=['POST'])
def getQuestionnair():
	data = request.data    
	print(">>",data)  
	y = json.loads(data)
	print(y)
	print(y['file'])
	import os.path
	file_path='./static/Questionnaire/'+y['file']
	file_exists = os.path.exists(file_path)
	if(file_exists==True):
		f = open(file_path)
		file_data = json.load(f)
		print(file_data)
		server_reponse['ack']="file found successfully"
		server_reponse['data']=file_data
		'''
		print("found")
		f = open(file_path)
		print("found1")
		file_data = json.load(f)
		print(file_data)
		server_reponse['ack']="file found successfully"
		server_reponse['data']=file_data
		'''
	else:
		server_reponse['ack']="file not found"
	
	return jsonify(server_reponse)



@app.route('/questionnaire', methods=['POST'])
def questionnaire():
	basepath = './static/Questionnaire'
	flist=""
	first=0
	for entry in os.scandir(basepath):
		if entry.is_dir():    # skip directories
			continue
		else:
			if first==0:
				first=1
				flist=entry.name
			else:
				flist=flist+","+entry.name
	print("files=", flist) # use entry.path to get the full path of this entry, or use entry.name for the base filename
	if (len(flist)>0):
		server_reponse['ack']="List of files found"; server_reponse['data']=flist
	else:
		server_reponse['ack']="no files found"
	return jsonify(server_reponse)


# simple method
def listfiles2(role):
	if (role == "user"):
		basepath = './static/results'
	if (role == "admin"):
		basepath = './static/Questionnaire'
	flist=""
	first=0
	for entry in os.scandir(basepath):
		if entry.is_dir():    # skip directories
			continue
		else:
			if first==0:
				first=1
				flist=entry.name
			else:
				flist=flist+","+entry.name
	print("files=", flist) # use entry.path to get the full path of this entry, or use entry.name for the base filename
	if (len(flist)>0):
		server_reponse['ack']="List of files found"; server_reponse['data']=flist
	else:
		server_reponse['ack']="no files found"
	return flist

@app.route('/filter', methods=['POST'])
def filter():
	data_file=""
	data = request.data    
	print(">>",data)  
	y = json.loads(data)
	print(y)
	#print(y['role'])
	
	if (y['action']=="filter"):
		print("1")
		if(len(y['questionnaire']) > 0 and len(y['client']) ==0):
			print("2")
			sql="SELECT * FROM aflatoxin.answer_sheet WHERE qp_tittle= %s;" ;values=( y['questionnaire'],)   
			mycursor.execute(sql,values)      
			
		if(len(y['questionnaire']) == 0 and len(y['client']) >0):
			print("3")
			sql="SELECT * FROM aflatoxin.answer_sheet WHERE client_name= %s;" ;	values=( y['client'],)  
			
			mycursor.execute(sql,values)   
		if(len(y['questionnaire']) > 0 and len(y['client']) >0):
			print("4")
			sql="SELECT * FROM aflatoxin.answer_sheet WHERE qp_tittle= %s AND client_name= %s;" ;	
			values=( y['questionnaire'],y['client'],)      
			
		print("5", sql, values)
		myresult = mycursor.fetchall()
		print(">>",myresult)
		for x in myresult:
			print(x)
			data_file=data_file+x[2]+","
			data_file[:-1];		print(data_file)
	else:
		if(y['role']=='user'):
			filenames=listfiles2(y['role']);		print(filenames)
			files=filenames.split(",");		data_file=""
			for x in files:
				if y['username'] in x:
					data_file=data_file+x+","
			data_file[:-1];		print(data_file)
		
		if(y['role']=='admin'):
			sql="SELECT * FROM aflatoxin.question_papers WHERE author= %s  ;" 
			values=( y['username'],)      
			mycursor.execute(sql,values)
			myresult = mycursor.fetchall()
			
			if(len(myresult)>0):
				print("myres:",myresult)
				print("myres:",myresult[0])
				print("myres:",myresult[0][0])
				admin_filename=[]; 
				for x in myresult:
					print(x)
					admin_filename.append(x[2])
					print(admin_filename)
				list_of_file=listfiles2(y['role'])
				files=list_of_file.split(",");
				print(files)
				data_file=""
				for f in admin_filename:
					print(f)
					if f in files:
						data_file=data_file+f+","
						print(data_file)
				data_file[:-1];		print(data_file)
		
	if (len(data_file)>0):
		server_reponse['ack']="file found successfully"
		server_reponse['data']=data_file
	else:
		server_reponse['ack']="No file found "	

	return server_reponse

@app.route('/aggregated_data', methods=['POST'])
def get_aggregated_data():
	# Read and aggregate JSON files
	data = request.data  
	print(">>",data)        
	reg_data = json.loads(data)
	print(reg_data['data'])

	aggregated_data = []
	file_paths=[]
	basepath = './static/results'
	for entry in os.scandir(basepath):
		if entry.is_dir():    # skip directories
			continue
		else:
			if(entry.name in reg_data['data']):
				file_paths.append(basepath+'/'+entry.name)
		#	#if first==0:
		#		first=1
		#		flist=entry.name
		#	else:
		#		file_paths.append(basepath+'/'+entry.name)

	#file_paths = ['file1.json', 'file2.json', 'file3.json']
	for file_path in file_paths:
		with open(file_path, 'r') as json_file:
			data = json.load(json_file)
			print("-----------")
			print(data)
			aggregated_data.append(data)
	print(">>", aggregated_data)
	# Return the aggregated data as a JSON response
	return jsonify(aggregated_data)
 
@app.route('/get_video')
def get_video():
    video_path = './static/video/TV Documentary.mp4'  # Replace with the actual path to your video file
    
    # Use the send_file function to send the video file in the response
    return send_file(video_path, as_attachment=True)

@app.route('/about_data',methods=['POST'])
def about_data():
	
    response={"ack": """
 Development and implementation of sustainable strategies to improve food-safety and retain nutritional values by reducing fungal infestation 
and aflatoxin contamination in the food-chain in Kenya as model region for Sub-Saharan Africa <br> <br>
Multidisciplinary cooperation project between German and African research institutions <br><br>
A fungal toxin that is one of the most toxic natural substances is the subject of the international cooperation project AflaZ: Aflatoxin. 
The research work in the project, coordinated by the Institute for Safety and Quality in Fruit and Vegetables (MRI), ranges from infestation 
of corn plants in the field by aflatoxin-producing fungi, the occurrence of aflatoxins in soils, to insects that spread the fungal spores, 
to cow's milk, in which toxic derivatives of aflatoxin are found.  <br>
AflaZ, which stands for "Zero Aflatoxin", is a research project in which strategies are being developed to reduce the aflatoxin contamination 
of foods frequently consumed in Kenya. Together with scientists from the Friedrich-Löffler-Institut (FLI), the Julius Kühn-Institut (JKI), 
the University of Koblenz-Landau (UKL), and two project partners in Kenya, KALRO (Kenya Agricultural and Livestock Research Organization) 
and EAFF (Eastern African Farmers Federation), the formation of aflatoxin on maize and the contamination of milk resulting from the ingestion 
of contaminated feed maize are being studied. Both foods are popular and widely consumed in sub-Saharan Africa. The project is coordinated 
by PD Dr. Markus Schmidt-Heydt, Institute for Safety and Quality in Fruits and Vegetables at MRI's Karlsruhe site. In addition, two other 
MRI institutes at the Detmold and Kiel sites are involved. 

"""}
    #try:
    data = request.data  
    print(">>",data)        
    
    return jsonify(response)

if __name__ == '__main__':
   app.run(host='0.0.0.0')

   '''
   
version: '2'
services:
  flask:
    build: .
    ports:
      - 8000:5000
    depends_on:
      - db
  db:
    image: mysql:5.7
    environment:
      - MYSQL_ROOT_PASSWORD=root
      - MYSQL_DATABASE=aflatoxin
    volumes:
      - ./data:/var/lib/mysql
      - ./dump.sql:/docker-entrypoint-initdb.d/dump.sql

   '''
   