

var json_demo = {
    questions: [
        {
            name: "name",
            type: "text",
            title: "Please enter your name:",
            placeHolder: "Jon Snow",
            isRequired: true
        }, {
            name: "birthdate",
            type: "text",
            inputType: "date",
            title: "Your birthdate:",
            isRequired: true
        }, {
            name: "color",
            type: "text",
            inputType: "color",
            title: "Your favorite color:"
        }, {
            name: "email",
            type: "text",
            inputType: "email",
            title: "Your e-mail:",
            placeHolder: "jon.snow@nightwatch.org",
            isRequired: true,
            validators: [
                {
                    type: "email"
                }
            ]
        }
    ]
};

var test_json = {
    "pages": [
        {
        "name": "page1",
        "elements": [
            {
            "type": "radiogroup",
            "name": "What is your gender?",
            "imageLink": "",
            "choices": [
                ' Male.', ' Female.', ' Prefer not to say.'
            ]
            },
            {
            "type": "checkbox",
            "name": "In which district/s is/are your farm/s located?",
            "imageLink": "",
            "choices": [
                ' Baringo', ' Bomet', ' Bungoma', ' Busia', ' Elgeyo-Marakwet', ' Embu', ' Garissa', ' Homa Bay', ' Isiolo', ' Kajiado', ' Kakamega', ' Kericho', ' Kiambu', ' Kilifi', ' Kirinyaga', ' Kisii', ' Kisumu', ' Kitui', ' Kwale', ' Laikipia', ' Lamu', ' Machakos', ' Makueni', ' Mandera', ' Marsabit', ' Meru', ' Migori', ' Mombasa (County)', " Murang'a", ' Nairobi (County)', ' Nakuru', ' Nandi', ' Narok', ' Nyamira', ' Nyandarua', ' Nyeri', ' Samburu', ' Siaya', ' Taita–Taveta', ' Tana River', ' Tharaka-Nithi', ' Trans-Nzoia', ' Turkana', ' Uasin Gishu', ' Vihiga', ' Wajir', ' West Pokot', ' Prefer not to say'
            ]
            },
            {
            "choices": [' Baringo', ' Bomet', ' Bungoma', ' Busia', ' Elgeyo-Marakwet', ' Embu', ' Garissa', ' Homa Bay', ' Isiolo', ' Kajiado', ' Kakamega', ' Kericho', ' Kiambu', ' Kilifi', ' Kirinyaga', ' Kisii', ' Kisumu', ' Kitui', ' Kwale', ' Laikipia', ' Lamu', ' Machakos', ' Makueni', ' Mandera', ' Marsabit', ' Meru', ' Migori', ' Mombasa (County)', " Murang'a", ' Nairobi (County)', ' Nakuru', ' Nandi', ' Narok', ' Nyamira', ' Nyandarua', ' Nyeri', ' Samburu', ' Siaya', ' Taita–Taveta', ' Tana River', ' Tharaka-Nithi', ' Trans-Nzoia', ' Turkana', ' Uasin Gishu', ' Vihiga', ' Wajir', ' West Pokot', ' Prefer not to say'],
            "name": "In which district/s is/are your farm/s located?",
            "type": "checkbox"
            },
            {
            "choices": [' < 5 ha', ' 5 - 50 ha', ' > 50 ha', ' Prefer not to say'],
            "name": "How large is your total farming area?",
            "type": "radiogroup"
            },
            {
            "choices": [' Harvesting with clean containers', ' Harvesting with clean tools / machines', ' Not harvesting plants with damaged fruits / grains', ' Frequently checking of crop moisture prior harvest ', ' Avoiding contact of crops with soil', ' None of these'],
            "name": "What do you do during harvest? Multiple selections possible.",
            "type": "checkbox"
            },
            {
            "choices": [' Yes, most time of year', ' Yes, but lifestock only graze on crop residues post-harvest', ' No'],
            "name": "Do livestock graze on your agricultural land (including livestock grazing on crop residue)?",
            "type": "radiogroup"
            },
            {
            "choices": [' Organic fertilization', ' Mineral fertilization', ' In situ green manuring by growing pulse crops like cowpea, horse gram, sunn hemp etc.', ' Irrigation (maybe ', ' Conventional tillage', ' Conservation tillage', ' AflaSafe', ' PushPull', ' Crop rotation', ' Fungicides', ' Insecticides', ' Herbicides', ' Organic mulching (straw, crop residues etc)', ' Plastic mulching', ' Intercropping', ' Crop residue removal', ' None of these'],
            "name": "What agricultural practices do you apply? Multiple selections possible.",
            "type": "checkbox"
            },
            {
            "choices": [' Yes', ' No'],
            "name": "Have you ever heard of mycotoxins (e.g. aflatoxins, Fusarium toxins)?",
            "type": "radiogroup"
            },
            {
            "choices": [' Reduced production of milk or eggs', ' Reduced weight gain and growth', ' Reduced feeding', ' Causes fever', ' Causes diarrhea or other gastrointestinal disturbances', ' Creates ulcers and sores', ' Causes death ', ' Damage the foetus ', ' Damage the juveniles', ' No / minimal risk to animal health'],
            "name": "Do you think that feeding moldy plant material (crops and crop residues) can have harmful effects on livestock and if so, what are they? Multiple selections possible.",
            "type": "checkbox"
            },
    

        ],
        "title": "Aflatoxin/Quiz 1",
        "description": "Basic questionnaire for aflatoxin",
        "Author": "sahique"
        }
    ]
    }

/*
window.survey = new Survey.Model(test_json);

survey
    .onComplete
    .add(function (result) {
        document
            .querySelector('#surveyResult')
            .textContent = "Result JSON:\n" + JSON.stringify(result.data, null, 3);
    });
    survey.onComplete.add(function (sender) {
        console.log("obtaing results");
        console.log(JSON.stringify(sender.data));
     
        var collected_answers={ "title":test_json.pages[0].title, "user":localStorage.getItem("user_name") } 
        console.log("**************",collected_answers)
        collected_answers.answers=sender.data;
        console.log(JSON.stringify(collected_answers));
        document.querySelector('#surveyResult').textContent = "Result JSON:\n" + JSON.stringify(collected_answers, null, 3);
        ///////////////////////// Sending data to back end
        var send_data=JSON.stringify(collected_answers);
        console.log("sending data");
        var end_point = server_ip+"collect_data";
    
        const getData = async(url=end_point,api_method="POST",api_body=send_data) => {     
            var response;
            response = await fetch(url,{
                method: api_method,
                body: api_body, // string or object
                headers : { 
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            });
            
            if (response.ok) {
                const raps = await response.json();
                console.log("raps:", raps)
                myresult = raps;
            }
            return (myresult);

            
        }
        getData().then(data => {
        console.log(data); alert(data.ack);} );
        ////////////////////////
    });

$("#surveyElement").Survey({model: survey});

*/

/*

window.survey = new Survey.Model(json);

survey
    .onComplete
    .add(function (result) {
        document
            .querySelector('#surveyResult')
            .textContent = "Result JSON:\n" + JSON.stringify(result.data, null, 3);
    });

$("#surveyElement").Survey({model: survey});

*/