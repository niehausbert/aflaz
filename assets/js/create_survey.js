Survey
    .StylesManager
    .applyTheme("modern");


    var demo_json = {
        questions: [
            {
                name: "name",
                type: "text",
                title: "Please enter your name:",
                placeHolder: "Jon Snow",
                isRequired: true
            }, {
                name: "birthdate",
                type: "text",
                inputType: "date",
                title: "Your birthdate:",
                isRequired: true
            }, {
                name: "color",
                type: "text",
                inputType: "color",
                title: "Your favorite color:"
            }, {
                name: "email",
                type: "text",
                inputType: "email",
                title: "Your e-mail:",
                placeHolder: "jon.snow@nightwatch.org",
                isRequired: true,
                validators: [
                    {
                        type: "email"
                    }
                ]
            }
        ]
    };

function create_survey4json(json) {
  $("#surveyElement").html("undefined survey");
  //window.survey = new Survey.Model(demo_json);
  window.survey = new Survey.Model(json);
  window.survey
      .onComplete
      .add(function (result) {
        $("#showresult4json").val(JSON.stringify(result.data, null, 3));
          document
              .querySelector('#surveyResult')
              .textContent = "Result JSON:\n" + JSON.stringify(result.data, null, 3);
      });

      survey.onComplete.add(function (sender) {
        console.log("obtaing results");
        console.log(JSON.stringify(sender.data));
       /* let text;
        let person = prompt("Please enter your name or id:", "");
        if (person == null || person == "") {
          text = "User cancelled the prompt.";
        } else {        
            var collected_answers={ "title":json.pages[0].title, "user":person } 
            collected_answers.answers=sender.data;
            console.log(JSON.stringify(collected_answers));
            document.querySelector('#surveyResult').textContent = "Result JSON:\n" + JSON.stringify(collected_answers, null, 3);
            ///////////////////////// Sending data to back end
            var send_data=JSON.stringify(collected_answers);
            console.log("sending data");
            var end_point = "http://127.0.0.1:5000/collect_data";
            
            const getData = async(url=end_point,api_method="POST",api_body=send_data) => {     
                var response;
                response = await fetch(url,{
                    method: api_method,
                    body: api_body, // string or object
                    headers : { 
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                    }
                });
                
                if (response.ok) {
                    const raps = await response.json();
                    console.log("raps:", raps)
                    myresult = raps;
                }
                return (myresult);
    
                
            }
            getData().then(data => {
            console.log(data)} );
        }*/

        var collected_answers={ "title":json.pages[0].title, "user":localStorage.getItem("user_name") } 
    console.log("**************",collected_answers)
    collected_answers.answers=sender.data;
    console.log(JSON.stringify(collected_answers));
    document.querySelector('#surveyResult').textContent = "Result JSON:\n" + JSON.stringify(collected_answers, null, 3);
    ///////////////////////// Sending data to back end
    var send_data=JSON.stringify(collected_answers);
    console.log("sending data");
    var end_point = server_ip+"collect_data";
    
    const getData = async(url=end_point,api_method="POST",api_body=send_data) => {     
        var response;
        response = await fetch(url,{
            method: api_method,
            body: api_body, // string or object
            headers : { 
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        });
        
        if (response.ok) {
            const raps = await response.json();
            console.log("raps:", raps)
            myresult = raps;
        }
        return (myresult);

        
    }
    getData().then(data => {
    console.log(data); alert(data.ack);} );
        ////////////////////////
        });
  $("#surveyElement").Survey({model: window.survey});


}
